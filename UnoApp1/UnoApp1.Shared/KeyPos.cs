﻿namespace UnoApp1
{
    public class KeyHolder
    {
        public KeyHolder()
        {
            Keys = new Key[14];
            for (int i = 0; i < 14; i++)
            {
                Keys[i] = new Key { list = Number, pos = i, present = false };
            }
            Lockable = false;
        }
        public int Number;
        public Key[] Keys;
        public bool Lockable;
    }
    public class Key
    {
        public string KeyId;
        public int list;
        public int pos;
        public bool present;
    }
}