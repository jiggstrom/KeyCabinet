﻿using System.Linq;
using Windows.UI.Xaml.Controls;

namespace UnoApp1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private CanControl canControl;

        public MainPage()
        {
            this.InitializeComponent();
            canControl = new CanControl();
            canControl.MessageRecieved+= MessageReceived;
            canControl.KeyInserted += KeyChanged;
            canControl.KeyRemoved += KeyChanged;
            canControl.InitDone += InitDone;
            //canControl.DoorOpened += Door_Opened;
            //canControl.DoorClosed += Door_Closed;

            canControl.StartListening(); 
            
        }

        internal async void Page_Loaded()
        {
            this.txtBlock.Text = "";
            await canControl.Initialize();
        }
        internal void Button2_Click()
        {
            canControl.StartListening();
        }
        internal async void Button3_Click()
        {
            await canControl.Unlock(1,0);
        }

        internal async void Button4_Click()
        {
            GpioControl.OpenDoor();
        }

        internal async void Button5_Click()
        {
            await canControl.Unlock(1, 1);
        }

        internal async void Button6_Click()
        {
            await canControl.Lock(1);
        }
        internal void MessageReceived(string msg)
        {
            System.Console.WriteLine(msg);
        }

        internal void KeyChanged(Key key)
        {
            InitDone(key.KeyId);
        }

        internal void InitDone(string msg)
        {            
            this.txtBlock.Text = $"{msg}\n";
            foreach (var item in canControl.Holders)
            {

                this.txtBlock.Text += $"List {item.Key} (låsbar: {(item.Value.Lockable ? "ja" : "nej")}) \n";
                this.txtBlock.Text += "------------------------------------------\n";
                foreach (var key in item.Value.Keys)
                {
                    if(key.present)
                        this.txtBlock.Text += $"Pos {key.pos,3} has key {key.KeyId}\n";
                    else
                        this.txtBlock.Text += $"Pos {key.pos,3} is empty\n";
                }

            }
        }
    }
}
