﻿using System;
using System.Linq;
using System.Device.Gpio;
using System.Threading;
using Iot.Device.SocketCan;

namespace UnoApp1
{
    public static partial  class GpioControl
    {
        public static void OpenDoor()
        {
            Console.WriteLine("door opening");
            int pin = 5;
            using var controller = new GpioController(PinNumberingScheme.Logical);
            controller.OpenPin(pin, PinMode.Output);
            controller.Write(pin, true);
            Thread.Sleep(1000);
            controller.Write(pin, false);
        }

        public static void ToggleListPower()
        {
            int pin = 13;
            using var controller = new GpioController(PinNumberingScheme.Logical);
            controller.OpenPin(pin, PinMode.Output);
            controller.Write(pin, true);
            Thread.Sleep(800);
            controller.Write(pin, false);
        }

        public static void InitLists(byte listId)
        {
            sendData(0x000, new[] { listId });
        }
        public static void TypeQuery(byte listId)
        {
            sendData((uint)0x300 + listId, new[] { listId });
        }

        public static void LockUnlock(byte listId, byte[] keyLockStatus)
        {
            sendData((uint)0x700 + listId, keyLockStatus);
        }

        public static void LedStatus(byte listId, byte[] keyLedStatus)
        {
            sendData((uint)0x000 + listId, keyLedStatus);
        }

        private static void sendData(uint Id, byte[] data)
        {
            CanId id = new CanId()
            {
                Standard = Id
            };

            string dataAsHex = string.Join("", data.ToArray().Select((x) => x.ToString("X2")));
            //Console.WriteLine($"Sending frame: 0x{id.Value:X2}: {dataAsHex}");

            using (CanRaw can = new CanRaw())
            {
                byte[] buffer = data;
                can.WriteFrame(buffer, id);
            }
        }

        public static (string id,string data) WaitForMsg(CanRaw can)
        {
            
            byte[] buffer = new byte[8];

            if (can.TryReadFrame(buffer, out int frameLength, out CanId id))
            {
                Span<byte> data = new Span<byte>(buffer, 0, frameLength);
                //string type = id.ExtendedFrameFormat ? "EFF" : "SFF";
                string dataAsHex = string.Join("", data.ToArray().Select((x) => x.ToString("X2")));
                return ($"0x{id.Value:X2}", dataAsHex);
            }
            else
            {
                Console.WriteLine($"Invalid frame received!");
                return ("","");
            }
        }
    }
}
