﻿using Iot.Device.SocketCan;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading;

namespace UnoApp1
{
    public class CanListener
    {
        public CancellationTokenSource cToken;
        private static bool IsStarted = false;

        public void Start(Progress<(string, string)> progressCallback)
        {
            if (!IsStarted)
            {
                cToken = new CancellationTokenSource();
                var t = new Thread(this.DoWork);
                t.Start(progressCallback);
            }
            IsStarted = true;
        }

        public void Stop()
        {
            if (IsStarted && cToken != null)
            {
                cToken.Cancel();
                Thread.Sleep(1500);
            }
        }

        /*************************************/
        //This code runs in a separate thread
        /*************************************/
        public void DoWork(object progressPar)
        {
            IProgress<(string, string)> progress = progressPar as IProgress<(string,string)>;
            try
            {
                using (CanRaw can = new CanRaw())
                {
                    while (!cToken.IsCancellationRequested)
                    {
                        var sRet = GpioControl.WaitForMsg(can);
                        progress.Report(sRet);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                IsStarted = false;
            }
        }
        /*************************************/
        /*************************************/
    }
}
