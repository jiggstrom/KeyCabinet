﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UnoApp1
{
    public partial class CanControl
    {
        CanListener listener;
        public event Action<string> InitDone;
        public event Action<Key> KeyInserted;
        public event Action<Key> KeyRemoved;
        public event Action<string> DoorOpened;
        public event Action<string> DoorClosed;
        public event Action<string> MessageRecieved;

        public Dictionary<int,KeyHolder> Holders = new Dictionary<int,KeyHolder>();
        private ConcurrentQueue<(string,string)> Messages = new ConcurrentQueue<(string,string)>();
        private Timer timer;

        public CanControl()
        {
            timer = new Timer(processMessages, null, TimeSpan.Zero, TimeSpan.FromMilliseconds(300));
        }

        private void processMessages(object state)
        {
            //Console.WriteLine($"{DateTime.Now.Ticks}:Process msgs.");
            while (!Messages.IsEmpty)
            {
                (string msg,string data) result;
                if (Messages.TryDequeue(out result)){
                    //Console.WriteLine($"{DateTime.Now.Ticks}:Msg fnd: {result.Item1}:{result.Item2}");

                    switch (result.msg)
                    {
                        case "0x00":
                            int list = int.Parse(result.data);
                            if (Holders.ContainsKey(list)) Holders.Remove(list);
                            MessageRecieved.Invoke($"List {list} cleared.");
                            break;
                        case "0x300":
                            var initKey = ParseKeyData(result.data);
                            if (AddKey(initKey))
                            {
                                MessageRecieved?.Invoke($"Pos {initKey.list}:{initKey.pos} has key {initKey.KeyId}");
                            }
                            else
                            {
                                MessageRecieved?.Invoke($"Error: Pos {initKey.list}:{initKey.pos} allready has key {Holders[initKey.list].Keys[initKey.pos].KeyId}");
                            }
                            break;
                        case "0x200":
                            var insertedKey = ParseKeyData(result.data);
                            if (AddKey(insertedKey))
                            {
                                KeyInserted?.Invoke(insertedKey);
                                MessageRecieved?.Invoke($" {insertedKey.KeyId} inserted to {insertedKey.list}:{insertedKey.pos}");
                            }
                            else
                            {
                                MessageRecieved?.Invoke($"Error: Key inserted, but {insertedKey.list}:{insertedKey.pos} allready has key {Holders[insertedKey.list].Keys[insertedKey.pos].KeyId}");
                            }
                            break;
                        case "0x201":
                            var removedKey = ParseKeyData(result.data);
                            if (RemoveKey(removedKey))
                            {
                                KeyRemoved?.Invoke(removedKey);
                                MessageRecieved?.Invoke($" {removedKey.KeyId} removed from {removedKey.list}:{removedKey.pos}");
                            }
                            else
                            {
                                MessageRecieved?.Invoke($"Error: Key removed, but {removedKey.list}:{removedKey.pos} supposed to be empty.");
                            }
                            break;
                        case var s when s.StartsWith("0x5"):
                            var statusList = int.Parse(s.Substring(3, 2), System.Globalization.NumberStyles.HexNumber);
                            if(result.data.Length == 2)
                            {
                                //Ack from lock/unlock
                            }
                            else if(result.data.Length == 4)
                            {
                                if(result.data.Substring(0,2) == "02")
                                {
                                    Holders[statusList].Lockable = true;
                                    MessageRecieved?.Invoke($"List {statusList} är låsbar.");
                                }
                                else
                                {
                                    Holders[statusList].Lockable = true;
                                    MessageRecieved?.Invoke($"List {statusList} är inte låsbar.");
                                }
                            }
                            break;
                        default:
                            MessageRecieved?.Invoke($"{result.msg} - {result.data}");
                            break;
                    }
                }
            }
        }

        private Key ParseKeyData(string data)
        {
            try {
                int list = int.Parse(data.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
                int present = int.Parse(data.Substring(2, 1), System.Globalization.NumberStyles.HexNumber);
                string key = data.Substring(4);
                int pos = int.Parse(data.Substring(3, 1), System.Globalization.NumberStyles.HexNumber)-1;
                return new Key
                {
                    list = list,
                    KeyId = key,
                    pos = pos,
                    present = present == 1
                };
            }
            catch(Exception)
            {
                return new Key();
            }        
        }

        private bool AddKey(Key key)
        {
            if (!Holders.ContainsKey(key.list))
            {
                Holders.Add(key.list, new KeyHolder { Number = key.list });
            }
            
            var curr = Holders[key.list].Keys[key.pos];

            if (curr.present == false)
            {
                Holders[key.list].Keys[key.pos] = key;
                return true;
            }
            return false;
        }

        private bool RemoveKey(Key key)
        {
            if (!Holders.ContainsKey(key.list)) return false;

            var curr = Holders[key.list].Keys[key.pos];
            
            if (curr.present == true)
            {
                Holders[key.list].Keys[key.pos] = new Key { pos = key.pos, list = key.list, present=false };
                return true;
            }
            return false;
        }

        public async Task Initialize()
        {
            GpioControl.ToggleListPower();
            var list = 1;
            var go = true;

            while (go)
            {
                go = false;
                //Console.WriteLine($"{DateTime.Now.Ticks}:init {list} begin. Holders={Holders.Count}");
                GpioControl.InitLists((byte)list);
                await Task.Delay(2000);
                //Console.WriteLine($"{DateTime.Now.Ticks}:init {list} dn. Holders={Holders.Count}");
                if (Holders.ContainsKey(list))
                {
                    GpioControl.TypeQuery((byte)list);
                    await Task.Delay(600);
                    //Console.WriteLine($"{DateTime.Now.Ticks}:typed {list} dn. Holders={Holders[list].Lockable}");
                    if (Holders[list].Lockable)
                    {
                        await Lock(list);
                        await Task.Delay(300);
                    }
                    list++;
                    go = true;
                }
            }

            InitDone?.Invoke("");
            MessageRecieved?.Invoke($"Init final {Holders.Count}");
        }

        public async Task Unlock(int list, int key)
        {
            byte[] keys = new byte[7];

            int ix;
            int m;
            if (key > 0) {
                ix = (int)key / 2;
                m = (key % 2) * 15 + 1;
            }
            else
            {
                ix = 0;
                m = 1;
            }

            keys[ix] = (byte)m;

            GpioControl.LockUnlock((byte)list, keys);
            //for (int i = 0; i < 50; i++)
            //{
                await Task.Delay(500);
            //}
            GpioControl.LedStatus((byte)list, keys);

        }

        public async Task Lock(int list)
        {
            byte[] keys = new byte[7];

            GpioControl.LockUnlock((byte)list, keys);
            //for (int i = 0; i < 50; i++)
            //{
                await Task.Delay(500);
            //}
            GpioControl.LedStatus((byte)list, keys);

        }
        public void StartListening()
        {
            if(listener == null)
            {
                listener = new CanListener();
            }

            var progress = new Progress<(string,string)>(OnStringRecieved);
            listener.Start(progress);
        }

        private void OnStringRecieved((string,string) obj)
        {
            //Console.WriteLine($"{DateTime.Now.Ticks}:Msg rcvd {obj.Item1}:{obj.Item2}");
            Messages.Enqueue(obj);
        }
    }
}
