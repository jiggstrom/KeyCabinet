﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnoApp1
{
    public partial class CanControl
    {
        public event Action<string> InitDone;
        public event Action<Key> KeyInserted;
        public event Action<Key> KeyRemoved;
        public event Action<string> DoorOpened;
        public event Action<string> DoorClosed;
        public event Action<string> MessageRecieved;
        //public Queue<(string, string)> Messages = new Queue<(string, string)>();
        public Dictionary<int, KeyHolder> Holders = new Dictionary<int, KeyHolder>();

        public async Task Initialize()
        {
        }
        public void StartListening()
        {
        }
        public async Task Unlock(int list, int key)
        {

        }
        public async Task Lock(int list)
        {

        }
    }
}
